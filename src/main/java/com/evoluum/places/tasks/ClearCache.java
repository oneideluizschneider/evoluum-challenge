package com.evoluum.places.tasks;


import com.evoluum.places.util.Loggable;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.CacheManager;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class ClearCache {

    @Loggable
    private static Logger log;

    @Autowired
    private CacheManager cache;

    @Scheduled(cron = "0 0 0 * * *")
    void start() {
        log.info("Iniciando limpeza do cache...");
        for (String name : cache.getCacheNames()) {
            cache.getCache(name).clear(); // clear cache by name
        }
        log.info("Finalizado limpeza do cache...");
    }

}