package com.evoluum.places.configuration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Configuration
@PropertySource("classpath:application.properties")
public class ApplicationProperties {

    @Value("${url.endpoint.estado}")
    private String endpointEstado;

    @Value("${url.endpoint.municipio}")
    private String endpointMunicipio;

    @Value("${url.endpoint.municipios.todos}")
    private String endpointMunicipioTodos;

}