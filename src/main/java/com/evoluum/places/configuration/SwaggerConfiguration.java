package com.evoluum.places.configuration;

import com.evoluum.places.dto.LocalidadeDTO;
import com.fasterxml.classmate.TypeResolver;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfiguration {
    
    private final TypeResolver typeResolver;

    @Autowired
    public SwaggerConfiguration(TypeResolver typeResolver) {
        this.typeResolver = typeResolver;
    }

    @Bean
    public Docket api() { 
        return new Docket(DocumentationType.SWAGGER_2)  
          .select()                                  
          .apis(RequestHandlerSelectors.basePackage("com.evoluum.places.controller"))          
          .paths(PathSelectors.regex("/.*"))                      
          .build()
          .additionalModels(typeResolver.resolve(LocalidadeDTO.class))
          .apiInfo(apiInfo());                                           
    }
    
    private ApiInfo apiInfo() {        
        return new ApiInfoBuilder()
                .title("API para consulta de Municípios")
                .description("API para consultar todos os Municípios do Brasil. Todos os endpoints podem ser analizados e também testados conforme documentação abaixo.")
                .version("v1.0")                
                .contact(new Contact("Oneide Luiz Schneider", "https://github.com/OneideLuizSchneider", "oneidels@gmail.com"))
                .build();
    }

}