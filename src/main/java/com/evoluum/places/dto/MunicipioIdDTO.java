package com.evoluum.places.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor(access = AccessLevel.PUBLIC)
@ApiModel(description = "Todos os detalhes do retono dos IDs dos municípios por nome.")
public class MunicipioIdDTO {
    @ApiModelProperty(notes = "ID do município")
    private int id;
}