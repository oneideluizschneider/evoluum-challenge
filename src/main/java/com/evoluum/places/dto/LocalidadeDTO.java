package com.evoluum.places.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor(access = AccessLevel.PUBLIC)
@ApiModel(description = "Todos os detalhes do retono das localidades(municípios)")
public class LocalidadeDTO {
	@ApiModelProperty(notes = "ID do estado")
	private int idEstado;
	@ApiModelProperty(notes = "Sigla do estado")
	private String siglaEstado;
	@ApiModelProperty(notes = "Nome da região")
	private String regiaoNome;
	@ApiModelProperty(notes = "Nome da cidade")
	private String nomeCidade;
	@ApiModelProperty(notes = "Nome da Mesoregião")
	private String nomeMesorregiao;   
	@ApiModelProperty(notes = "Nome formatado Cidade/UF")
	private String nomeFormatado;
	
	public LocalidadeDTO(Estado estado, Municipio municipio) {
		this.idEstado = estado.getId();
		this.siglaEstado = estado.getSigla();
		this.regiaoNome = estado.getRegiao().getNome();
		this.nomeCidade = municipio.getNome();
		this.nomeMesorregiao = municipio.getMicrorregiao().getMesorregiao().getNome();
		this.nomeFormatado = municipio.getNome() + "/" + estado.getSigla();
	}
}