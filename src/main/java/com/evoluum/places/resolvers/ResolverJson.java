package com.evoluum.places.resolvers;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.http.HttpServletResponse;
import com.evoluum.places.dto.LocalidadeDTO;
import com.evoluum.places.util.Loggable;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;

import ch.qos.logback.classic.Logger;

@Component
public class ResolverJson implements Resolver {

    @Loggable
    private static Logger log;

    private static final String CONTENT_TYPE = MediaType.APPLICATION_JSON_VALUE;
    private static final String CHARSET = "UTF-8";
    
    @Override
    public void processar(List<LocalidadeDTO> listLocalidades, HttpServletResponse httpResponse) {
        try {
			log.info("Iniciado gerar Json de retorno...");                                    
            PrintWriter out = httpResponse.getWriter();
            httpResponse.setContentType(CONTENT_TYPE);
            httpResponse.setCharacterEncoding(CHARSET);
            out.print(new ObjectMapper().writeValueAsString(listLocalidades));
            out.flush();               
			log.info("Finalizado gerar Json de retorno...");                                    
		} catch (IOException e) {
			log.info("Falha ao gerar Json de retorno.");
			log.error("ERRO = " + e.getMessage());
		}

    }

}