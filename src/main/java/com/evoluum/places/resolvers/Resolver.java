package com.evoluum.places.resolvers;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import com.evoluum.places.dto.LocalidadeDTO;

public interface Resolver {

    public void processar(List<LocalidadeDTO> list, HttpServletResponse httpResponse);

}