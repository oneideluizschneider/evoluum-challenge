package com.evoluum.places.resolvers;

import com.evoluum.places.controller.TipoRetornoEnum;

import org.springframework.stereotype.Component;

@Component
public class ResolverFactory {

    public Resolver getResolver(TipoRetornoEnum tipo) {
        switch (tipo) {
        case CSV:
            return new ResolverCsv();
        case JSON:
            return new ResolverJson();
       
        default:
            return null;
        }
    }
    
}