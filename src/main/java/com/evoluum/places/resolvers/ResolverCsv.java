package com.evoluum.places.resolvers;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.List;
import javax.servlet.http.HttpServletResponse;
import com.evoluum.places.dto.LocalidadeDTO;
import com.evoluum.places.util.Loggable;

import org.slf4j.Logger;
import org.springframework.stereotype.Component;

@Component
public class ResolverCsv implements Resolver {

    @Loggable
    private static Logger log;

    private static final String CONTENT_TYPE = "text/csv";

    @Override
    public void processar(List<LocalidadeDTO> list, HttpServletResponse httpResponse) {
        try {
            log.info("Iniciou conversao para CSV...");
            byte[] data = processar(list);
            log.info("Finalizou conversao para CSV...");

            httpResponse.setHeader("Content-Disposition", "attachment; filename=\"municipios.csv\"");
            httpResponse.setContentType(CONTENT_TYPE);
            httpResponse.setContentLength(data.length);
            httpResponse.getOutputStream().write(data);
        } catch (IOException e) {
            log.info("Erro ao gerar CSV...");
            log.error("ERRO = " + e.getMessage());
        }
    }

    private byte[] processar(List<LocalidadeDTO> list) {
        String separator = ";";
        StringBuilder sb = new StringBuilder();
        sb.append("idEstado,siglaEstado,regiaoNome,nomeCidade,nomeMesorregiao,nomeFormatado\n")
                .append(System.lineSeparator());
        for (LocalidadeDTO localidadeDto : list) {
            sb.append(localidadeDto.getIdEstado()).append(separator);
            sb.append(localidadeDto.getSiglaEstado()).append(separator);
            sb.append(localidadeDto.getRegiaoNome()).append(separator);
            sb.append(localidadeDto.getNomeCidade()).append(separator);
            sb.append(localidadeDto.getNomeMesorregiao()).append(separator);
            sb.append(localidadeDto.getNomeFormatado()).append(separator);
            sb.append(System.lineSeparator());
        }
        return sb.toString().getBytes(StandardCharsets.UTF_8);
    }

}