package com.evoluum.places.service;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import com.evoluum.places.controller.TipoRetornoEnum;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.evoluum.places.dto.MunicipioIdDTO;
import com.evoluum.places.resolvers.Resolver;
import com.evoluum.places.resolvers.ResolverFactory;

@Service
public class LocalidadeService {

    @Autowired
    private ApiCallLocalidadeService apiCallLocalidadeService;

    public void todos(TipoRetornoEnum tipoRetorno, HttpServletResponse httpResponse) {
        Resolver resolver = new ResolverFactory().getResolver(tipoRetorno);
        resolver.processar(apiCallLocalidadeService.getTodos(), httpResponse);
    }

    public List<MunicipioIdDTO> getMunicipiosPorNome(String nomeMunicipio) {
        return apiCallLocalidadeService.getByNomeMunicipio(nomeMunicipio);
    }

}