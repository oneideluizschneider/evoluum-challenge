package com.evoluum.places.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;

import com.evoluum.places.configuration.ApplicationProperties;
import com.evoluum.places.dto.Estado;
import com.evoluum.places.dto.LocalidadeDTO;
import com.evoluum.places.dto.Municipio;
import com.evoluum.places.dto.MunicipioIdDTO;
import com.evoluum.places.util.Loggable;

@Service
public class ApiCallLocalidadeService {

    @Loggable
    private static Logger log;

    @Autowired
    private ApplicationProperties appProperties;

    @Autowired
    private RestTemplate restTemplate;

    @HystrixCommand(fallbackMethod = "defaultEmptyLocalidade", commandProperties = { @HystrixProperty(name = "execution.isolation.thread.timeoutInMilliseconds", value = "30000") })
    public List<LocalidadeDTO> getTodos() {        
        log.info("Iniciou carregar lista de estados...");
        List<Estado> listEstados = getEstados();
        log.info("Finalizou de carregar lista de estados...");

        log.info("Iniciou carregar lista de municipios de cada estado.");
        List<LocalidadeDTO> listLocalidades = getLocalidades(listEstados);
        log.info("Finalizou carregar lista de municipios de cada estado.");

        return listLocalidades;
    }

    @HystrixCommand(fallbackMethod = "defaultEmptyMunicipio", commandProperties = { @HystrixProperty(name = "execution.isolation.thread.timeoutInMilliseconds", value = "20000") })
    @Cacheable("municipioKey")
    public List<MunicipioIdDTO> getByNomeMunicipio(String nomeCidade) {  
        log.info("Iniciou carregar lista de municipios...");
        List<Municipio> todosOsMunicipios = Arrays.asList(
                restTemplate.getForEntity(appProperties.getEndpointMunicipioTodos(), Municipio[].class).getBody());
        log.info("Finalizou carregar lista de municipios...");

        log.info("Iniciou filtrar/localizar municipio...");
        List<MunicipioIdDTO> muncipiosRetorno = todosOsMunicipios.stream()
                .filter(municipio -> municipio.getNome().toUpperCase().equals(nomeCidade.toUpperCase()))
                .map(municipio -> new MunicipioIdDTO(municipio.getId())).collect(Collectors.toList());
        log.info("Finalizou filtrar/localizar municipio...");

        return muncipiosRetorno;
    }

    private List<Estado> getEstados() {
        return Arrays.asList(restTemplate.getForEntity(appProperties.getEndpointEstado(), Estado[].class).getBody());
    }

    private List<LocalidadeDTO> getLocalidades(List<Estado> listEstados) {
        List<LocalidadeDTO> listLicalidades = new ArrayList<>();
        listEstados.forEach(uf -> {
            List<Municipio> listaMunicipios = Arrays.asList(restTemplate
                    .getForEntity(String.format(appProperties.getEndpointMunicipio(), uf.getId()), Municipio[].class)
                    .getBody());
            listaMunicipios.forEach(dto -> listLicalidades.add(new LocalidadeDTO(uf, dto)));
        });

        return listLicalidades;
    }

    private List<LocalidadeDTO> defaultEmptyLocalidade() {
        return new ArrayList<LocalidadeDTO>();
    }

    private List<MunicipioIdDTO> defaultEmptyMunicipio(String nomeCidade) {
        return new ArrayList<MunicipioIdDTO>();
    }

}