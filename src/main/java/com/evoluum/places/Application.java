package com.evoluum.places;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableScheduling;

@EnableCaching
@EnableScheduling
@EnableCircuitBreaker
@ComponentScan(basePackages = {"com.evoluum.places"})
@SpringBootApplication
public class Application {	

	public static void main(String[] args) {		
		SpringApplication.run(Application.class, args);
	}

}
