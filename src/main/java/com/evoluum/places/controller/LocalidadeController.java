package com.evoluum.places.controller;

import java.util.List;

import javax.servlet.http.HttpServletResponse;

import com.evoluum.places.dto.MunicipioIdDTO;
import com.evoluum.places.service.LocalidadeService;
import com.evoluum.places.util.Loggable;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping(value = "v1/localidade")
@Api(value = "Localidade", description = "Endpoint de localidades")
public class LocalidadeController {

	@Loggable
	private static Logger log;

	@Autowired
	private LocalidadeService localidadeService;

	@GetMapping("/todos/{tipoRetorno}")
	@ApiOperation(value = "Retorna todos os municípios por tipo, por exemplo 'CSV', e se o timeout passar de 30 segundos, se passar disso irá retornar uma lista vazia.")
	@ApiResponses(value = {
		@ApiResponse(code = 200, message = "Sucesso"),
		@ApiResponse(code = 404, message = "Recurso não encontrado")
	})
	public ResponseEntity<Object> getAll(@PathVariable TipoRetornoEnum tipoRetorno, HttpServletResponse httpResponse) {
		log.info("Iniciado processamento para para todos os dados, tipo: " + tipoRetorno.getValue());		
		localidadeService.todos(tipoRetorno, httpResponse);
		log.info("Finalizado processamento para para todos os dados, tipo: " + tipoRetorno.getValue());		

		return ResponseEntity.status(httpResponse.getContentType() == null ? HttpStatus.NOT_FOUND : HttpStatus.OK).body(null);
	}

	@GetMapping("/municipio/{nomeMunicipio}")
	@ApiOperation(value = "Busca de município por nome, por exemplo 'Chapecó', todos os retornos serão salvos em cache, e toda meia noite são renovados.")
	@ApiResponses(value = {
		@ApiResponse(code = 200, message = "Sucesso"),
		@ApiResponse(code = 404, message = "Recurso não encontrado")
	})
	public ResponseEntity<List<MunicipioIdDTO>> getMunicipioPorNome(@PathVariable String nomeMunicipio) {
		log.info("Iniciado processamento para a o minícipio " + nomeMunicipio);
		List<MunicipioIdDTO> municipios = localidadeService.getMunicipiosPorNome(nomeMunicipio);
		log.info("Finalizado processamento para a o minícipio " + nomeMunicipio);

		return ResponseEntity.status(HttpStatus.OK).body(municipios);
	}
}