package com.evoluum.places.controller;

public enum TipoRetornoEnum {

    CSV("CSV"), JSON("JSON");

    private final String value;

    TipoRetornoEnum(final String newValue) {
        value = newValue;
    }

    public String getValue() {
        return value;
    }

}