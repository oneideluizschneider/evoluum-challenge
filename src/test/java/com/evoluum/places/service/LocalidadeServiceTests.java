package com.evoluum.places.service;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;

import java.util.ArrayList;

import javax.servlet.http.HttpServletResponse;

import com.evoluum.places.resolvers.ResolverFactory;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.SpyBean;

@SpringBootTest
class LocalidadeServiceTests {

	@SpyBean
	private LocalidadeService service;
	
	@MockBean
	private ResolverFactory resolverFactory;
	
	@MockBean
	private HttpServletResponse httpResponse;

	@MockBean
	private ApiCallLocalidadeService api;
	
	@Test
	public void isTrue() {				
		assertTrue(service != null);
		assertTrue(resolverFactory != null);
		assertTrue(httpResponse != null);
		assertTrue(api != null);
	}
		
	@Test
	public void testMunicipioId() {
		doReturn(new ArrayList<>()).when(api).getByNomeMunicipio(any());
		assertNotNull(service.getMunicipiosPorNome("Chapecó"));
	}

}
