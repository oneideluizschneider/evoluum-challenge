package com.evoluum.places.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.when;

import com.evoluum.places.configuration.ApplicationProperties;
import com.evoluum.places.dto.Estado;
import com.evoluum.places.dto.Mesorregiao;
import com.evoluum.places.dto.Microrregiao;
import com.evoluum.places.dto.Municipio;
import com.evoluum.places.dto.Regiao;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;


@SpringBootTest
class ApiCallLocalidadeServiceTests {

	@SpyBean
	private ApiCallLocalidadeService apiCallLocalidadeService;
	
	@MockBean
	private RestTemplate restTemplate;
	
	@Autowired
	private ApplicationProperties appProperties;
		
	private Estado[] estados;
	private Municipio[] municipios;

	private String nomeEstado = "Santa Catarina";
	private int idEstado = 42;

	@BeforeEach
	public void init(){
		mockData();
	}

    private void mockData() {
		estados = new Estado[1];
		municipios = new Municipio[1];

		Municipio municipio = new Municipio();
		Microrregiao microrregiao = new Microrregiao();
		Mesorregiao mesorregiao = new Mesorregiao();	
		mesorregiao.setNome("OESTE");
		microrregiao.setMesorregiao(mesorregiao);		
		municipio.setMicrorregiao(microrregiao);
		municipio.setNome(nomeEstado);
		municipios[0] = municipio;		

		Estado estado = new Estado();
		estado.setNome(nomeEstado);
		estado.setId(idEstado);
		Regiao regiao = new Regiao();		
		regiao.setNome("OESTE");
		estado.setRegiao(regiao);		
		estados[0] = estado;
	}

	@Test
	public void testEstados() {			
		initMockApiData();
		assertNotNull(apiCallLocalidadeService.getTodos());
	}

	private void initMockApiData() {			
		ResponseEntity<Estado[]> responseEntityUf = new ResponseEntity<Estado[]>(estados, HttpStatus.OK);
		when(restTemplate.getForEntity(appProperties.getEndpointEstado(), Estado[].class)).thenReturn(responseEntityUf);
		
		ResponseEntity<Municipio[]> responseEntityCity = new ResponseEntity<Municipio[]>(municipios, HttpStatus.OK);					
		when(restTemplate.getForEntity(String.format(appProperties.getEndpointMunicipio(), idEstado), Municipio[].class)).thenReturn(responseEntityCity);		
	}

	@Test
	public void testGetByMunicipioNome() {			
		mockDataGetByMunicipioNome();
		assertNotNull(apiCallLocalidadeService.getByNomeMunicipio(nomeEstado));
	}

	private void mockDataGetByMunicipioNome() {
		ResponseEntity<Municipio[]> responseEntityCity = new ResponseEntity<Municipio[]>(municipios, HttpStatus.OK);
		when(restTemplate.getForEntity(appProperties.getEndpointMunicipioTodos(), Municipio[].class)).thenReturn(responseEntityCity);	
	}


	@Test
	public void contextLoads() {				
		assertThat(apiCallLocalidadeService).isNotNull();		
		assertThat(restTemplate).isNotNull();
		assertThat(appProperties).isNotNull();
	}

}
