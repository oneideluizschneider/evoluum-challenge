package com.evoluum.places.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;

import javax.servlet.http.HttpServletResponse;

import com.evoluum.places.service.LocalidadeService;

import org.springframework.http.HttpStatus;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;

@SpringBootTest
class LocalidadeControllerTests {

	@Autowired
	private LocalidadeController controller;

	@MockBean
	private LocalidadeService service;

	@MockBean
	private HttpServletResponse httpResponse;

	@Test
	public void testGetAll() {
		when(httpResponse.getContentType()).thenReturn(MediaType.APPLICATION_JSON_VALUE);
		assertEquals(HttpStatus.OK, controller.getAll(TipoRetornoEnum.JSON, httpResponse).getStatusCode());

		when(httpResponse.getContentType()).thenReturn("text/csv");
		assertEquals(HttpStatus.OK, controller.getAll(TipoRetornoEnum.CSV, httpResponse).getStatusCode());
	}

	@Test
	void isTrue() {
		assertTrue(controller != null);
		assertTrue(service != null);
		assertTrue(httpResponse != null);
	}

}
