package com.evoluum.places.resolvers;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.http.HttpServletResponse;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.SpyBean;

@SpringBootTest
class ResolverCsvTests {

	@SpyBean
	private ResolverCsv resolverCsv;
	
	@MockBean
	private HttpServletResponse httpResponse;
	
	@Test
	public void isTrue() {				
		assertTrue(resolverCsv != null);
		assertTrue(httpResponse != null);	
	}

	@Test
	public void testProcessar() throws IOException {
		doNothing().when(resolverCsv).processar(any(), any());;
		resolverCsv.processar(new ArrayList<>(), httpResponse);
	}

}
