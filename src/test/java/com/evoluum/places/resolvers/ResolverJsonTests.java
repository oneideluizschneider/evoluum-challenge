package com.evoluum.places.resolvers;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.doReturn;

import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;

import javax.servlet.http.HttpServletResponse;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

@SpringBootTest
class ResolverJsonTests {

	@Autowired
	private ResolverJson resolverJson;
	
	@MockBean
	private HttpServletResponse response;
	
	@Test
	public void isTrue() {				
		assertTrue(resolverJson != null);
		assertTrue(response != null);
	}


	@Test
	public void testResolverJson() throws IOException {
		PrintWriter printWriter = new PrintWriter("testResolverJson.json");
		doReturn(printWriter).when(response).getWriter();
		resolverJson.processar(new ArrayList<>(), response);
		printWriter.close();
		Files.delete(Paths.get("testResolverJson.json"));
	}

}