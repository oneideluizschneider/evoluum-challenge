FROM gradle:5.6.3-jdk12
 
ADD src src
ADD build.gradle .
ADD settings.gradle .
 
RUN gradle build

FROM openjdk:12-jdk-alpine

COPY --from=0 /home/gradle/build/libs/evoluum-challenge-v1.0.jar evoluum-challenge-v1.0.jar
 
EXPOSE 8080
 
ENTRYPOINT exec java $JAVA_OPTS -jar /evoluum-challenge-v1.0.jar