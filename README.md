# evoluum-challenge #

### Executando projeto ###

Há duas formas de executar o projeto.

* Build com gradle e java

* Docker

Com gradle:
```
./gradlew clean 
./gradlew build 
./gradlew bootRun 
```
Com docker:
```
docker build -t evoluumchallenge . 
docker run --name evoluum-challenge -p 8080:8080 evoluumchallenge 
```
### Acessando documentação ###

Após iniciar o projeto, na URL abaixo é possivel analisar e testar os endpoints da API:

http://localhost:8080/api/swagger-ui.html


### Endpoints da API ###

Para retornar todos os dados temos esse endpoint, onde pode-se usar dois formatos de retorno.
JSON e CSV.

```
/api/v1/localidade/todos/{tipoRetorno}
```

Para retornar somente os ID dos municípios temos esse endpoint. 
Todos os retornos serão salvos em cache e limpos todos os dias a meia noite.
```
/api/v1/localidade/municipio/{nomeMunicipio}
```